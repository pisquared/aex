#include <stdio.h>


int main() {
  // Little endian encoding of chars з, д, р
  // 0xb7d0, 0xb4d0, 0x80d1
  char buf[8] = {0xd0, 0xb7,0xd0,0xb4,0xd1,0x80, 0x0};
  printf("%s\n", buf);
  printf("%d\n", EOF);
  printf("%lu\n", sizeof(char));
  printf("%lu\n", sizeof(short));
  printf("%lu\n", sizeof(int));
  printf("%lu\n", sizeof(long));
  printf("%lu\n", sizeof(float));
  printf("%lu\n", sizeof(double));
  printf("%lu\n", sizeof(void *));
  return 0;
}
