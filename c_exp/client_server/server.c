#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include<string.h>

#define MAXBUF		1024

char RESPONSE[] = "HTTP/1.1 200 OK\r\nContent-Length: 2\r\nContent-Type: text/plain\r\n\r\nab";
char buffer[MAXBUF];

int main()
{
  printf("pid: %d\n", getpid());
    int listen_fd, comm_fd;
 
    struct sockaddr_in servaddr;
 
    listen_fd = socket(AF_INET, SOCK_STREAM, 0);
 
    bzero( &servaddr, sizeof(servaddr));
 
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htons(INADDR_ANY);
    servaddr.sin_port = htons(1234);
 
    bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr));
 
    listen(listen_fd, 130);
 
    while(1) {
        comm_fd = accept(listen_fd, (struct sockaddr*) NULL, NULL);
        //send(comm_fd, buffer, recv(comm_fd, buffer, MAXBUF, 0), 0);
        send(comm_fd, RESPONSE, strlen(RESPONSE)+1, 0);
        close(comm_fd);
    }
}
