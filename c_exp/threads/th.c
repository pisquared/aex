#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void *entry(void *param) {
  printf("THREAD pid: %d\n", getpid());
  getchar();
  printf("THREAD after getchar: %d\n", getpid());
}

int main() {
  printf("MAIN   before create: %d\n", getpid());
  pthread_t thread0;
  pthread_create(&thread0, NULL, entry, NULL);
  printf("MAIN   after create: %d\n", getpid());
  getchar();
  printf("MAIN   after getchar: %d\n", getpid());
  pthread_join(thread0, NULL);
  printf("MAIN   after join: %d\n", getpid());

  return 0;
}
