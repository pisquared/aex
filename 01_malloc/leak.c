#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void) {
  printf("pid: %d\n", getpid());
  int *p = (int *) malloc(1024 * 1024 * 1024 * sizeof(int));
  printf("addr: %p\n", p);
  getchar();
  return 0;
}
