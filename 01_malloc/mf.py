import random
import sys

# 0 free memory
# - unalocated memory
# <num[1-9]> meta data
# X alocated memory

AVAILABLE_MEMORY = 64
memory = ["0"] * AVAILABLE_MEMORY

# ============================================
# SYSTEM LEVEL CALLS
# ============================================

def brk(addr):
    global memory
    if addr >= AVAILABLE_MEMORY:
        raise Exception("RUN OUT OF MEMORY!")
    memory[addr] ="|"

def sbrk(incr):
    global memory
    boundary = memory.index('|')
    if incr == 0:
        return boundary
    brk(boundary + incr)

def execve():
    global memory
    brk(1)

# ============================================
# MALLOC AND FREE IMPLEMENTATION
# ============================================

METADATA_SIZE = 2
# First element of the metadata is t|f - taken or freed memory
# Second element is 1:8 - bytes taken by malloc

def _naive_malloc(size):
    """
    Naive malloc - add to the end of memory
    """
    global memory
    print("malloc with size %s" % size)
    boundary = sbrk(0)
    # increment the boundary
    sbrk(size + METADATA_SIZE)
    chunk = "t" + str(size) + "X" * size
    pos = boundary + METADATA_SIZE
    memory[boundary:boundary + size + METADATA_SIZE] = list(chunk)
    return pos

def _find_first_malloc(size):
    return size

MALLOC_STRATEGY = _naive_malloc

def malloc(size):
    return MALLOC_STRATEGY(size)

def free(pointer):
    global memory
    print("free %s" % pointer)
    length = memory[pointer - 1]
    memory[pointer - 2] = "f"
    for i in range(pointer, pointer + int(length)):
        memory[i] = '-'


choices = [malloc, free]

# Helper functions
def print_memory():
    global memory
    for i, byte in enumerate(memory):
        if i % 4 == 0:
            sys.stdout.write(" ")
        sys.stdout.write(byte)
    print

# TEST
# =============================
pointers = []

execve()
print_memory()
for i in range(30):
    if not pointers:
        choice = malloc
    else:
        choice = random.choice(choices)

    if choice == malloc:
        s = random.randint(1, 8)
        p = choice(s)
        pointers.append(p)
    elif choice == free:
        p = pointers.pop(random.randrange(len(pointers)))
        choice(p)
    print_memory()
