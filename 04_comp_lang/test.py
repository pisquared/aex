import signal
import sys

i = 0

def iter():
    global i
    while True:
        i = i+1

def signal_handler(signal, frame):
    global i
    print(i)
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.pause()
iter()
