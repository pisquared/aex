#include <stdio.h>
#include <stdlib.h>

int INITIAL_CAPACITY = 2;

struct ilist {
  int size;
  int capacity;
  int *buckets;
};

typedef struct ilist ilist;


ilist ilist_init() {
 ilist list;
 list.size = 0;
 list.capacity = INITIAL_CAPACITY;
 list.buckets = malloc(sizeof(int) * list.capacity);
 return list;
}

void ilist_add(ilist *list, int value) {
  if (list->size == list->capacity) {
    list->capacity = (list->capacity) * 2;
    list->buckets = realloc(list->buckets, list->capacity * sizeof(int));
    printf("--- new list capacity is %d\n", list->capacity);
  }
  *(list->buckets + list->size) = value;
  list->size++;
}

void ilist_set(ilist *list, int pos, int value) {
  if (pos > list->size) {
    printf("Cannot set list element beyond current size: %d", list->size);
    return;
  }
  *(list->buckets + pos) = value;
}

int ilist_get(ilist *list, int pos) {
  return *(list->buckets + pos);
}

int fill(int n) {
  ilist my_list = ilist_init();
  for (int i = 0; i < n; i++) {
    printf("Filling bucket %d\n", i);
    ilist_add(&my_list, i);
  }

  printf("=========\n");

  for (int j = 0; j < n; j++) {
    printf("Value of %d is %d\n", j, ilist_get(&my_list, j));
  }
}

int main(int argc, char **argv) {
  if (argc != 2) {
    printf("Usage: f.o <n>\n");
    return 1;
  }

  int n = atoi(argv[1]);
  fill(n);

  return 0;
}
