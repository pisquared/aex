# take the emoji heart character U+2764 point
u_heart = '\u2764'

# encoding in utf-8 to see the bytes
'\u2764'.encode('utf-8')
b'\xe2\x9d\xa4'

# now these bytes are the following binary:
[bin(x) for x in '\u2764'.encode('utf-8')]
['0b11100010', '0b10011101', '0b10100100']

# now take the encoded bytes in UTF-8 and create a character:
bytes([0b11100010, 0b10011101, 0b10100100]).decode('utf-8')
'❤'
