class Node(object):
    def __init__(self, value=0):
        self.edges = []
        self.value = value

    def register_edge(self, edge):
        self.edges.append(edge)

    def connected_nodes(self):
        for edge in self.edges:
            if edge.start_node == self:
                yield edge.end_node, edge.weight
            elif edge.end_node == self:
                yield edge.start_node, edge.weight

    def __repr__(self):
        return '%s' % self.value

class Edge(object):
    def __init__(self, start_node, end_node, weight):
        self.start_node = start_node
        self.start_node.register_edge(self)

        self.end_node = end_node
        self.end_node.register_edge(self)

        self.weight = weight

class Graph(object):
    def __init__(self, nodes, edges):
        self.nodes = nodes
        self.edges = edges

    def find_shortest_path(self, node1, node2):
        path = []
        # assign every node to inf
        unvisited_nodes = set()
        current_node = node1
        for node in self.nodes:
            node.visited = False
            unvisited_nodes.add(node)
            if node == node1:
                node.distance = 0
                continue
            node.distance = float('inf')


        while True:
            print 'exmining node %r' % current_node
            for node, distance in current_node.connected_nodes():
                if node.visited:
                    continue
                print node, node.distance, current_node.distance + distance, node.distance > current_node.distance + distance
                if node.distance > current_node.distance + distance:
                    node.distance = current_node.distance + distance

            current_node.visited = True
            unvisited_nodes.remove(current_node)
            min_dist = float('inf')
            for candidate in unvisited_nodes:
                print 'cd: %s' % candidate.distance
                if candidate.distance < min_dist:
                    min_dist = candidate.distance
                    current_node = candidate

            if node2.visited or min_dist == float('inf'):
                break
        return node2.distance

n1 = Node(1)
n2 = Node(2)
n3 = Node(3)
n4 = Node(4)
n5 = Node(5)
n6 = Node(6)

e1 = Edge(n1, n2, 7)
e2 = Edge(n1, n3, 9)
e3 = Edge(n2, n3, 10)
e4 = Edge(n2, n4, 15)
e5 = Edge(n3, n4, 11)
e6 = Edge(n4, n5, 6)
e7 = Edge(n5, n6, 9)
e8 = Edge(n1, n6, 14)
e9 = Edge(n3, n6, 2)

g = Graph(nodes=[n1,n2,n3,n4,n5,n6],
          edges=[e1,e2,e3,e4,e5,e6,e7,e8,e9])

print g.find_shortest_path(n1, n5)
