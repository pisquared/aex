# coding: utf-8
"""
Ели е наследила от баба си (Нора) винарска изба. В нея има N бутилки вино, наредени в редица. За простота ще ги номерираме с числата от 0 до N-1, включително. Техните начални цени са неотрицателни цели числа, които са ни дадени в масива P[]. Цената на i-тата бутилка е дадена в Pi. Колкото повече отлежават бутилките, толкова по-скъпи стават те. Ако бутилка k е отлежала Х години, нейната цена ще бъде X*Pk.

В завещанието си бабата на Ели е поискала всяка година внучка ѝ да продава по една от тях, като избира или най-лявата или най-дясната останала. Каква е максималната сума пари, която Ели може да спечели, ако продава бутилките в най-добрия за нея ред? Считаме, че бутилките са отлежавали 1 година, когато бива продадена първата от тях.

Например ако имаме 4 бутилки с цени {P0 = 1, P1 = 4, P2 = 2, P3 = 3}, оптималното решение би било да продаде бутилките в реда {0, 3, 2, 1} за печалба 1*1 + 2*3 + 3*2 + 4*4 = 29.
"""

prices = [1,4,3]
dyn = [[-1] * 3] * 3

def recurse(left, right, year):
    print ' ' * year + '%s -- l: %s r: %s -- %s' % (year, left, right, prices[left:right])
    if left > right:
        return 0
    if dyn[left][right] != -1:
        return dyn[left][right]

    print ' ' * year + 'taking left'
    win_left = recurse(left + 1, right, year + 1) + year * prices[left]
    print ' ' * year + 'left p: %s' % win_left
    print ' ' * year + '---'
    print ' ' * year + 'taking right'
    win_right = recurse(left, right - 1, year + 1) + year * prices[right]
    print ' ' * year + 'right p: %s' % win_right

    dyn[left][right] = max(win_right, win_left)
    print ' ' * year + 'p: %s' % dyn[left][right]
    return dyn[left][right]

recurse(0, len(prices) - 1, 1)
print dyn
