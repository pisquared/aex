DEBUG = 1

def print_debug(W, S, m, i):
    print "m = %s, i = %s " % (m, i)
    print S
    print ' ' * m + W
    print ' ' * (m + i) + '^'
    print '-' * len(S)

def kmp_table(W):
    """ Construct a table of backtracking.
    Returns a list of integers with length of W.
    At each position answers What is the longest prefix of W that matches
    at the current position.
    """
    T = [0] * len(W)
    pos = 1
    cnd = 0
    while pos < len(W):
        if W[pos] == W[cnd]:
            T[pos] = cnd + 1
            cnd += 1
        else:
            T[pos] = 0
            cnd = 0
        pos += 1
    return T

def kmp(W, S):
    # denoting the position within S where the prospective match for W begins
    m = 0
    # denoting the index of the currently considered character in W
    i = 0
    # table of backtracking integers
    T = kmp_table(W)

    # the alg
    word_len = len(W)
    str_len = len(S)
    while m + i < str_len:
        if DEBUG:
            print_debug(W, S, m, i)
        if S[m + i] == W[i]:
            if i == word_len - 1:
                return m
            i = i + 1
        else:
            if i > 0:
                m = m + i - T[i - 1]
            else:
                m = m + 1
            i = 0
    return -1

print kmp_table("ABCDABD")
assert kmp("ABCDABD", "ABC ABCDAB ABCDABCDABDE") == 15
