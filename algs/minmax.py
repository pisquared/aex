class Node(object):
    def __init__(self, data):
        self.children = []
        self.parent = None
        self.data = data

    def add(self,child):
        self.children.append(child)
        return self


def minmax(node, depth, maxPlay):
    print "{}{} {}".format(' '*depth, node.data,maxPlay)
    if not depth:
        return node.data

    if maxPlay:
        best = -float("inf")
        for n in node.children:
            v = minmax(n, depth-1, False)
            best = max(n.data, v)
        print "{}>{} {}".format(' '*depth, best,maxPlay)
        return best
    else:
        best = float("inf")
        for n in node.children:
            v = minmax(n, depth-1, True)
            best = min(n.data, v)
        print "{}>{} {}".format(' '*depth, best,maxPlay)
        return best

root = Node(0)
root.add(Node(5)
        .add(Node(1))
        .add(Node(2)))
root.add(Node(10)
        .add(Node(4))
        .add(Node(6)))

print(minmax(root, 3, True))

