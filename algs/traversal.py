# NOTE: In Python 3.3 instead of `for x in gen: yield x` one can use `yield from`

tree = {
  'F': ['B', 'G'],
  'B': ['A', 'D'],
  'D': ['C', 'E'],
  'G': [None, 'I'],
  'I': ['H', None]
}

def pre_order(tree, start):
    if start:
        yield start
    if start in tree:
        for x in pre_order(tree, tree[start][0]): yield x
        for x in pre_order(tree, tree[start][1]): yield x

def in_order(tree, start):
    if start in tree:
        for x in in_order(tree, tree[start][0]): yield x
    if start:
        yield start
    if start in tree:
        for x in in_order(tree, tree[start][1]): yield x

def post_order(tree, start):
    if start in tree:
        for x in post_order(tree, tree[start][0]): yield x
        for x in post_order(tree, tree[start][1]): yield x
    if start:
        yield start

assert [a for a in pre_order(tree, 'F')] == ['F', 'B', 'A', 'D', 'C', 'E', 'G', 'I', 'H']
assert [a for a in in_order(tree, 'F')] == ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']
assert [a for a in post_order(tree, 'F')] == ['A', 'C', 'E', 'D', 'B', 'H', 'I', 'G', 'F']
