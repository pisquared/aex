import java.util.Arrays;

public class MergeSort {
  private static int[] swap(int[] a, int posx, int posy) {
    int temp = a[posx];
    a[posx] = a[posy];
    a[posy] = temp;
    return a;
  }

  private static int[] sort(int[] a) {
    System.out.println("sorting " + Arrays.toString(a));
    int length = a.length;
    if (length <= 2) {
      if (length == 2 && a[0] > a[1]) {
        a = swap(a, 0, 1);
      }
      System.out.println("--ret: " + Arrays.toString(a));
      return a;
    }
    int middle = length / 2;
    int[] left = sort(Arrays.copyOfRange(a, 0, middle));
    int[] right = sort(Arrays.copyOfRange(a, middle, length));
    int[] rv = new int[left.length + right.length];
    System.out.println("merging " + Arrays.toString(left) + " and " + Arrays.toString(right));
    for (int li = 0 , ri = 0, i = 0; i < left.length + right.length; i++) {
      System.out.println("- i:" + i + " li:" + li +  " ri:" + ri);
      if (li < left.length && ri < right.length && left[li] < right[ri]) {
        rv[i] = left[li];
        li++;
      } else {
        if (ri < right.length) {
          rv[i] = right[ri];
          ri++;
        } else {
          rv[i] = left[li];
          li++;
        }
      }
    }
    return rv;
  }

  public static void main(String[] argv) {
    int[] a = {4,5,2,3,1, 7, 8, 13, 17, 35, 5,8,12,9,2,1};
    System.out.println(Arrays.toString(sort(a)));
  }
}
