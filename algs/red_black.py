"""
tree rotation: https://en.wikipedia.org/wiki/Tree_rotation
red-black trees cases: http://pages.cs.wisc.edu/~skrentny/cs367-common/readings/Red-Black-Trees/
red-black trees vis: https://www.cs.usfca.edu/~galles/visualization/RedBlack.html
"""

class Node(object):
    def __init__(self, data):
        self.red = 1
        self.data = data
        self.left = None
        self.right = None
        self.parent = None

    def add(self, node):
        if node.data < self.data:
            if self.left is None:
                self.left = node
                node.parent = self
            else:
                self.left.add(node)
        else:
            if self.right is None:
                self.right = node
                node.parent = self
            else:
                self.right.add(node)
    
    def __str__(self):
        return '%s' % self.data

class Tree(object):
    def __init__(self, root):
        self.root = root
        root.red = 0

    def rotate(self, node, mode='l'):
        print "  {}({})".format("LR" if mode == 'l' else 'RR', node)
        parent = node.parent
        print "  parent -> {}".format(parent)
        pivot = node.right if mode == 'l' else node.left
        print "  pivot -> {}".format(pivot)
        beta = pivot.left if mode == 'l' else node.right
        print "  beta -> {}".format(beta)
        
        # assign pivots pointers
        if not parent:
            # this is root!
            self.root = pivot
        else:
            if parent.right == node:
                parent.right = pivot
            else:
                parent.left = pivot
        pivot.parent = parent

        # assign beta's pointers
        if mode == 'l': 
            node.right = beta
        else:
            node.left = beta
        if beta:
            beta.parent = node

        # assign node's pointers
        node.parent = pivot
        if mode == 'l': 
            pivot.left = node
        else:
            pivot.right = node

    def verify_rules(self, node):
       print "{} verify rules".format(node)
       parent = node.parent
       print "{} parent -> {}".format(node, parent)
       if not parent.parent:
           print "{} is root, recolor".format(parent)
           parent.red = 0
       else:
           print "{} is not root".format(parent)
           if parent.red:
               print "2. Violation of redness -  check other sibling"
               grandparent = parent.parent
               print "{} grandparent -> {}".format(node, grandparent)
               uncle = grandparent.left if grandparent.right == parent else grandparent.left
               print "{} uncle -> {}".format(node, uncle)
               if not uncle or not uncle.red:
                   print "2a uncle ({}) is black".format(uncle)
                   if parent.left == node:
                       if grandparent.left == parent:
                           print "{} -L-> {} -L-> {} => RR".format(grandparent, parent, node)
                           self.rotate(grandparent, 'r')
                           print "coloring parent ({}) black".format(parent)
                           parent.red = 0
                       else:
                           print "{} -R-> {} -L-> {} => RR LR".format(grandparent, parent, node)
                           self.rotate(parent, 'r')
                           self.rotate(grandparent, 'l')
                           print "coloring node ({}) black".format(node)
                           node.red = 0
                   else:
                       if grandparent.left == parent:
                           print "{} -L-> {} -R-> {} => LR RR".format(grandparent, parent, node)
                           self.rotate(parent, 'l')
                           self.rotate(grandparent, 'r')
                           print "coloring node ({}) black".format(node)
                           node.red = 0
                       else:
                           print "{} -R-> {} -R-> {} => LR".format(grandparent, parent, node)
                           self.rotate(grandparent, 'l')
                           print "coloring parent ({}) black".format(parent)
                           parent.red = 0
                   print "coloring grandparent ({}) red".format(grandparent)
                   grandparent.red = 1
               else:
                   print "2b uncle ({}) is red".format(uncle)
                   uncle.red = 0
                   parent.red = 0
                   if not grandparent == self.root:
                       grandparent.red = 1
                       self.verify_rules(grandparent)

    def add(self, node):
       # start at root
       print "ADDING {}".format(node)
       self.root.add(node)
       # node is now properly inserted in Binary tree
       # is parent red?
       self.verify_rules(node)
       return self

    def bfs(self):
        q = [self.root]
        while q:
            nq = list()
            for current in q:
                if not current:
                    print '***',
                    continue
                print "{}({})".format(current.data, current.red),
                nq.append(current.left) if current.left else nq.append(None)
                nq.append(current.right) if current.right else nq.append(None)
            print
            q = nq

def test_rot(seq, rot):
    print "===================="
    print seq
    tree = Tree(Node(seq[0]))
    for item in seq[1:]:
        tree.add(Node(item))
    tree.bfs()
    print '{} ---'.format(rot)
    if rot == 'LR':
        tree.rotate(tree.root, 'l')
    elif rot == 'RR':
        tree.rotate(tree.root, 'r')
    elif rot == 'LRRR':
        tree.rotate(tree.root.left, 'l')
        tree.rotate(tree.root, 'r')
    elif rot == 'RRLR':
        tree.rotate(tree.root.right, 'r')
        tree.rotate(tree.root, 'l')
    tree.bfs()

def test_basic_rots():
    test_rot([1,2,3], 'LR')
    test_rot([3,2,1], 'RR')
    test_rot([3,1,2], 'LRRR')
    test_rot([1,3,2], 'RRLR')


def test_seq(seq):
    print "===================="
    print seq
    tree = Tree(Node(seq[0]))
    for item in seq[1:]:
        tree.add(Node(item))
    tree.bfs()

test_seq([1])
test_seq([1, 2])
test_seq([1, 2, 3])
test_seq([1, 2, 3, 4])
test_seq([1, 2, 3, 4, 5])
test_seq([1, 2, 3, 4, 5, 6])
test_seq([1, 2, 3, 4, 5, 6, 7])
test_seq([1, 2, 3, 4, 5, 6, 7, 8])
test_seq([1, 2, 3, 4, 5, 6, 7, 8, 9])
test_seq([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
test_seq([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
test_seq([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
