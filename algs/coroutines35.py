#!/usr/bin/python3
# http://masnun.com/2015/11/20/python-asyncio-future-task-and-the-event-loop.html
import asyncio

# Coroutine - simultenous functions in python. 
# They `yield from` or `await` on blocking calls.
# Actually "blocking calls" must *all* be reimplemented in async library.
async def slow():
    await asyncio.sleep(1)
    return "Finished"

# callback from a task that takes a future
def done(future):
    print(future.result())

# get the main event loop
loop = asyncio.get_event_loop()

# create a task from the coroutine
task = loop.create_task(slow())

# add a callback
task.add_done_callback(done)

# run the eventloop
loop.run_until_complete(task)
