from collections import OrderedDict, deque

graph = {
    'A': set(['B', 'C']),
    'B': set(['D', 'E', 'A']),
    'C': set(['F', 'A']),
    'D': set(['B']),
    'E': set(['B', 'F']),
    'F': set(['C', 'E'])
}

def dfs(graph, start):
    # OrderedDict makes sure that element is checked
    # for membership in O(1) (set) but also remembers
    # the order in which it was inserted (like a list).
    path, stack = OrderedDict(), [start]
    while stack:
        print stack
        current = stack.pop()
        if current not in path:
            print current
            path[current] = ''
            vertices = graph[current]
            for vertice in vertices:
                stack.append(vertice)
    return path.keys()

def dfs_recursive(graph, start, path=[]):
    path = path + [start]
    for vertice in graph[start]:
        if vertice not in path:
            path = dfs_recursive(graph, vertice, path)
    return path


def bfs(graph, start):
    path, queue = OrderedDict(), deque([start])
    while queue:
        current = queue.popleft()
        if current not in path:
            path[current] = ''
            queue.extend(graph[current])
    return path.keys()

dfs(graph, 'A')
