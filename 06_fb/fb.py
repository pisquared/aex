import requests
from lxml import html

class Message(object):
    def __init__(self, name="", body="", sent_ts=""):
        self.name = name.encode('utf-8')
        self.body = body.encode('utf-8')
        self.sent_ts = sent_ts

class MessageThread(object):
    def __init__(self, name="", messages=None, url=""):
        self.name = name.encode('utf-8')
        self.messages = messages or list()
        self.url = url


BASE_URL = "https://mbasic.facebook.com" 
MESSAGES_URL = '/messages'
HEADERS = {'Cookie': 'datr=Ko6-WcwcG1z-iMeAB26zOXBV; sb=Ko6-WXDcBvTxqkYxR8gtygBl; locale=en_US; c_user=1457541242; xs=41%3A0eeLVoFJZ7Mm2w%3A2%3A1505660532%3A10625%3A5231; fr=0n4VQ7gs62kImWeBr.AWU89FMq_p6B4SKPkmUV-y2weWY.BZvo5w.08.AAA.0.0.BZvo5w.AWXBIGKi; pl=n'}
    
def get_message_threads():
    response = requests.get("{base}{loc}".format(base=BASE_URL, loc=MESSAGES_URL), 
                            headers=HEADERS)
    tree = html.fromstring(response.content)
    return tree

def parse_message_threads(tree):
    raw_messages = tree.xpath('//div[@id="root"]/div[1]//table//text()')[2:]
    links = tree.xpath('//div[@id="root"]/div[1]//table//a')[2:]
    message_threads = []
    for i in range(0, len(raw_messages) - 2, 3):
        message = Message(name=raw_messages[0+i],
                          body=raw_messages[1+i], 
                          sent_ts=raw_messages[2+i],
                          )
        message_thread = MessageThread(name=raw_messages[0+i],
                                       messages=[message],
                                       url=links[i/3].attrib['href'])
        message_threads.append(message_thread)
    return message_threads

def print_messages(message_threads):
    # print(repr(messages).decode('unicode-escape'))
    for i, message_thread in enumerate(message_threads):
        for message in message_thread.messages:
            print("[{i}] {name}".format(i=i, name=message.name, body=message.body))
            print("    {body}".format(name=message.name, body=message.body))
            print("    {sent_ts}".format(sent_ts=message.sent_ts))
            print("    {url}".format(url=message_thread.url))
        print("-" * 50)

if __name__ == "__main__":
    html_tree = get_message_threads()
    message_threads = parse_message_threads(html_tree)
    print_messages(message_threads)
