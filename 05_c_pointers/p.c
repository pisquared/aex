#include <stdio.h>

int strlen2(char *s) {
  int n;
  for (n = 0; *s != '\0'; s++)
    n++;
  return n;
}

int main(){ 
  int a[3] = {1,2,3};
  printf("%d\n", strlen2("Hello world"));
  printf("a      = %p\n", a);
  printf("&a     = %p\n", &a);
  printf("&a[0]  = %p\n", &a[0]);
  printf("a+1    = %p\n", a+1);
  printf("&a[1]  = %p\n", &a[1]);
  printf("a[0]   = %d\n", a[0]);
  printf("a[1]   = %d\n", a[1]);
  printf("*a     = %d\n", *a);
  printf("*(a+1) = %d\n", *(a+1));
  printf("*(a+2) = %d\n", *(a+2));

  char ames[] = "Hello";
  char *pmes = "Hello";
  printf("first:...\n");
  printf("%s\n", ames);
  printf("%s\n", pmes);
  
  ames[1] = 'f';
  // CANT DO THAT!!!: pmes[1] = 'f';
  
  printf("after first:...\n");
  printf("%s\n", ames);
  printf("%s\n", pmes);
  
}
